resource "aws_db_instance" "kpmg" {
  allocated_storage    = 10
  engine               = "mysql"
  engine_version       = "8.0"
  instance_class       = "db.t2.micro"
  name                 = "kpmg-db"
  username             = var.username
  password             = var.password
  parameter_group_name = "default.mysql8.0"
  kms_key_id           = data.aws_kms_key.by_alias.arn 
  vpc_security_group_ids = [aws_security_group.allow_ec2.id]
  skip_final_snapshot  = true
  deletion_protection  = true
}

#security group for RDS
resource "aws_security_group" "allow_ec2" {
  name        = "allow_ec2"
  description = "Allow ec2 instances inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    from_port = 3036
    to_port = 3036
    protocol = "tcp"
    cidr_blocks = [aws_security_group.allow_http.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "rds_sg"
  }
}

