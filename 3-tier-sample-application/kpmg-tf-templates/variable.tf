variable "template_name" {}
variable "ami_id" {}
variable "instance_type" {}
variable "vpc_zone_identifier" {
   type = list
   default =["subnet-908210f6","subnet-c80f96e9"]
}
variable "vpc_id" {}
variable "username" {
  description = "Database administrator username"
  type        = string
  sensitive   = true
}

variable "password" {
  description = "Database administrator password"
  type        = string
  sensitive   = true
}

