import json
import argparse
import sys



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Script support find the value of a key in a nested dictionary')
    # Required Arguments
    parser.add_argument("-o", "--object", required=True, 
                        help="nested dictionary for this operation")
    parser.add_argument("-k", "--key", required=True,type=str, 
                        help="key for this operation")
    args = parser.parse_args()
    object_input = args.object
    key_input = args.key.split('/')
    json_object = json.loads(object_input)
    for i in key_input:
        obj_dict = json_object[i]
        json_object = obj_dict

    print(f"object output result: {json_object}")

