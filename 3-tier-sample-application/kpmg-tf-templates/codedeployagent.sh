#!/bin/bash
yum update -y
yum install ruby -y
yum install wget -y
cd /tmp
wget https://aws-codedeploy-us-east-1.s3.us-east-1.amazonaws.com/latest/install
chmod +x ./install
./install auto
sudo service codedeploy-agent start
