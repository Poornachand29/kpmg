# README #


### What is this repository for? ###

* This is to create a sample web tier application and deploy via code pieline from source code
- Front end - Load balancer end point
- Web tier  - Auto scaling group
- Database tier - RDS MYSQL instance


### DEPLOYMNET ###

* Upload the sample web application to the source code repository
- index.html
- appspec.yaml - hooks to install the dependencies and run the start and stop script 
- scripts

* kpmg-tf-templates
- autoscaling.tf - includes launch templates and autoscaling group
- codepipeline.tf - stage1) download from source code, stage2) deploy to asg using code deploy
- codedeploy_service_role.tf - service role for code deploy
- codepipeline_service_role.tf - service role for code pipeline
- ec2_service_role - service role for ec2
- elb.tf - Load balancer
- rds.tf - MYSQL RDS instance and security group to allow ec2 to connect RDS on 3306


